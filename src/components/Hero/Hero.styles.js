import styled from "styled-components"
import colors from "styles/Colors"
import breakpoints from "styles/Breakpoints"

export const HeroWrapper = styled.div`
  height: 100%;
  width: 100%;
  position: relative;

  .ring,
  .circle {
    position: absolute;
    opacity: 0.1;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    transform: translateY(50%);

    circle,
    path {
      fill: ${colors.white};
    }

    &.ring {
      max-width: 750px;
      width: 100%;
    }

    &.circle {
      max-width: 400px;
      width: 50%;
    }
  }
`

export const HeroContent = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-between;
  align-items: center;
  top: 50%;
  left: 0;
  right: 0;
  margin: auto;
  padding: 0 15px;
  max-width: 1160px;
  transform: translateY(-50%);

  @media screen and (${breakpoints.sm}) {
    padding: 0 24px;
  }
`

export const HeroTextWrapper = styled.div`
  color: ${colors.white};
  
  @media screen and (${breakpoints.sm}) {
    max-width: 80%;
  }
  
  @media screen and (${breakpoints.md}) {
    max-width: 60%;
  }
`
