import React from "react"
import scrollTo from "gatsby-plugin-smoothscroll"

// Components
import HeroImage from "components/HeroImage"
import { DiscoverButton } from "components/Buttons"
import Ring from "icons/ring.svg"
import Circle from "icons/circle.svg"

// Hooks
import { useHeroQuery } from "hooks/useHeroQuery"

// Styles
import {
  HeroWrapper as Wrapper,
  HeroContent as Content,
  HeroTextWrapper as TextWrapper,
} from "./Hero.styles"

const Hero = () => {
  const {
    heroImage,
    heroImageText,
    heroImageParagraph,
    heroDiscoverText,
  } = useHeroQuery()

  return (
    <HeroImage title="heroImage" fluid={heroImage.childImageSharp.fluid}>
      <Wrapper id="hero">
        <DiscoverButton onClick={() => scrollTo("#about")}>
          {heroDiscoverText}
        </DiscoverButton>
        <Ring className="ring" />
        <Circle className="circle" />
        <Content>
          <TextWrapper>
            <h1>{heroImageText}</h1>
            <p>{heroImageParagraph}</p>
          </TextWrapper>
        </Content>
      </Wrapper>
    </HeroImage>
  )
}

export default Hero
