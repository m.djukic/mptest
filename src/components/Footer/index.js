import React from "react"
import { Link } from "gatsby"

// Components
import Container from "@material-ui/core/Container"
import FooterItems from "components/FooterItems"
import Ring from "icons/ring.svg"
import Circle from "icons/circle.svg"

// Hooks
import { useSiteConfigQuery } from "hooks/useSiteConfigQuery"

// Styles
import {
  Logo,
  FooterWrapper as Wrapper,
  FooterInner as Inner,
  FooterNavWrapper as NavWrapper,
} from "./Footer.styles"

const Layout = () => {
  const siteConfig = useSiteConfigQuery()

  return (
    <Wrapper id="footer">
      <Container maxWidth="xl">
        <Inner>
          <Ring className="ring" />
          <Circle className="circle" />
          <NavWrapper>
            <Link to="/">
              <Logo src={siteConfig.logo.publicURL} alt="logo" />
            </Link>
          </NavWrapper>
          <FooterItems />
        </Inner>
      </Container>
    </Wrapper>
  )
}

export default Layout
