import styled from "styled-components"
import colors from "styles/Colors"
import breakpoints from "styles/Breakpoints"

export const FooterWrapper = styled.div`
  height: 100%;
  width: 100%;
  background-color: ${colors.black};
`

export const FooterInner = styled.div`
  position: relative;
  overflow: hidden;
  padding: 20px 0;

  @media screen and (${breakpoints.sm}) {
    padding: 40px 0;
  }

  .ring,
  .circle {
    position: absolute;
    opacity: 0.1;
    z-index: 1;

    circle,
    path {
      fill: ${colors.white};
    }
  }

  .ring {
    max-width: 750px;
    width: 100%;
    top: 0;
    left: 0;
    transform: translateY(-50%);
  }

  .circle {
    max-width: 400px;
    width: 100%;
    bottom: 0;
    right: 0;
    transform: translateY(50%);
  }
`

export const Logo = styled.img`
  display: inline-block;
  height: 45px;

  @media screen and (max-width: 768px) {
    padding-bottom: 20px;
  }
`

export const FooterNavWrapper = styled.div`
  z-index: 2;

  @media screen and (max-width: 768px) {
    flex-wrap: wrap;
  }
`
