import React from "react"
import { Link } from "gatsby"

//Styles
import { NavWrapper, Nav } from "./Navigation.styles"

const Navigation = ({ items }) => (
  <NavWrapper className="textWhite">
    <Nav>
      {items.slice(0, 1).map(item => (
        <li key={item.id}>
          <Link to={item.link} activeClassName="active">
            {item.name}
          </Link>
        </li>
      ))}
    </Nav>
    <Nav>
      {items.slice(1, 3).map(item => (
        <li key={item.id}>
          <Link to={item.link} activeClassName="active">
            {item.name}
          </Link>
        </li>
      ))}
    </Nav>
  </NavWrapper>
)

export default Navigation
