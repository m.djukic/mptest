import styled from "styled-components"
import colors from "styles/Colors"
import breakpoints from "styles/Breakpoints"

export const NavWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: none;

  @media screen and (${breakpoints.md}) {
    display: flex;
    justify-content: space-between;
    align-content: center;
  }
`

export const Nav = styled.ul`
  display: flex;
  justify-content: space-between;
  align-items: center;

  li {
    height: 100%;
    padding: 0 10px;
  }

  a {
    display: flex;
    align-items: center;
    height: 100%;
    transition: 0.4s;
  }

  a:hover {
    color: ${colors.yellow};
    border-bottom: 1px solid ${colors.yellow};
    transition: 0.4s;
  }
`
