import styled from "styled-components"
import colors from "styles/Colors"

// Components
import Button from "@material-ui/core/Button"

export const PrimaryButton = styled(Button)`
  && {
    position: relative;
    background-color: transparent;
    text-transform: none;
    box-shadow: none;
    box-sizing: border-box;
    border-radius: 5px;
    padding: 10px 20px;
    margin-top: 40px;
    border: 1px solid ${colors.yellow};
    font-family: "Lato";
    overflow: hidden;
    z-index: 1;
    white-space: nowrap;

    &:before {
      content: "";
      position: absolute;
      box-sizing: border-box;
      height: 100%;
      width: 100%;
      top: 0;
      left: 0;
      z-index: -1;
      border-radius: 5px;
      color: ${colors.black};
      border: 1px solid ${colors.yellow};
      background-color: ${colors.yellow};
      transform: translateX(-100%);
      transition: transform 0.4s ease-in-out;
    }

    &:hover:before {
      transform: translateX(0%);
    }

    &.white {
      color: ${colors.white};
    }
  }
`

export const SecondaryButton = styled(Button)`
  && {
    position: relative;
    background-color: transparent;
    color: ${colors.white};
    text-transform: none;
    box-shadow: none;
    box-sizing: border-box;
    border-radius: 5px;
    padding: 10px 20px;
    margin-top: 30px;
    border: 1px solid ${colors.white};
    font-family: "Lato";
    overflow: hidden;
    z-index: 1;
    white-space: nowrap;
    transition: color 0.4s;

    &:before {
      position: absolute;
      content: "";
      box-sizing: border-box;
      height: 100%;
      width: 100%;
      top: 0;
      left: 0;
      z-index: -1;
      border-radius: 5px;
      border: 1px solid ${colors.white};
      background-color: ${colors.white};
      transform: translateX(-100%);
      transition: transform 0.4s ease-in-out;
    }
    &:hover {
      color: ${colors.black};
      transition: color 0.4s;

      &:before {
        transform: translateX(0%);
      }
    }
  }
`

export const DiscoverButton = styled(Button)`
  && {
    position: absolute;
    bottom: 100px;
    left: 50%;
    transform: translateX(-50%);
    margin: auto;
    z-index: 3;
    color: ${colors.white};
    background-color: transparent;
    text-transform: none;
    box-shadow: none;
    font-family: "Lato";
    transition: 0.2s;

    &:after {
      content: "";
      position: absolute;
      bottom: 0;
      left: 50%;
      height: 100px;
      width: 2px;
      background: ${colors.white};
      transform: translate(-50%, 100%);
      transition: 0.2s;
    }

    &:hover {
      color: ${colors.yellow};
      transition: 0.2s;

      &:after {
        background: ${colors.yellow};
        transition: 0.2s;
      }
    }
  }
`
