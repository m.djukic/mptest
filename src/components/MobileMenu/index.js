import React from "react"
import { Link } from "gatsby"
// Styles
import { Wrapper, Nav } from "./MobileMenu.styles"

const MobileMenu = ({ menuOpen, items }) => (
  <Wrapper menuOpen={menuOpen}>
    <Nav>
      {items.map(item => (
        <li key={item.id}>
          <Link to={item.link} activeClassName="active">
            {item.name}
          </Link>
        </li>
      ))}
    </Nav>
  </Wrapper>
)

export default MobileMenu
