import styled from "styled-components"
import colors from "styles/Colors"
import breakpoints from "styles/Breakpoints"


export const Wrapper = styled.div`
  display: block;
  position: fixed;
  right: ${props => (props.menuOpen ? "0px" : "-320px")};
  width: 320px;
  height: 100%;
  background: ${props => props.theme.colors.menuBackground};
  top: 0px;
  padding: 0 0 0 20px;
  transition: all 0.4s ease;
  z-index: 10;
  box-shadow: 2px 0px 10px 0px rgba(0, 0, 0, 0.3);
  box-sizing: border-box;
  border-left: 1px solid ${colors.white};

  @media screen and (${breakpoints.md}) {
    display: none;
  }
`

export const Nav = styled.ul`
  position: absolute;
  width: 100%;
  top: 70px;

  

  li {
    cursor: pointer;
    margin: 20px 0;


    &:hover a {
      color: ${colors.yellow};
      transition: all 0.2s;
    }

    a {
    color: ${props => props.theme.colors.textWhite};
  }
  }
`
