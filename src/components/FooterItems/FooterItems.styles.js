import styled from "styled-components"
import colors from "styles/Colors"

export const FooterItemsWrapper = styled.div`
  position: relative;
  color: ${colors.white};
  z-index: 2;

  li {
    padding-bottom: 5px;

    a {
      transition: 0.2s;
      cursor: pointer;

      &:hover {
        color: ${colors.yellow};
        transition: 0.2s;
      }
    }
  }
`
