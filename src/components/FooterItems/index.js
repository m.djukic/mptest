import React from "react"
import { Link } from "gatsby"

// Components
import { Box } from "@material-ui/core"

// Hooks
import { useFooterQuery } from "hooks/useFooterQuery"

// Styles
import { FooterItemsWrapper as Wrapper } from "./FooterItems.styles"

const FooterItems = () => {
  const FooterQuery = useFooterQuery()
  const items = FooterQuery.items

  return (
    <Wrapper>
      <Box>
        <h4>{FooterQuery.col1Header}</h4>
        <ul>
          {items.map(item => (
            <li className={item.id} key={item.id}>
              <Link to={item.link}>{item.name}</Link>
            </li>
          ))}
        </ul>
      </Box>

    </Wrapper>
  )
}

export default FooterItems
