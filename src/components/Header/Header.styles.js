import styled from "styled-components"
import colors from "styles/Colors"

export const HeaderWrapper = styled.div`
  background: ${props => props.theme.colors.menuBackground};
  color: ${colors.white};
  position: absolute;
  top: 0px;
  width: 100%;
  border-bottom: 1px solid ${colors.white};
  z-index: 999;
`

export const HeaderInner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-flow: row nowrap;
  height: 80px;
`

export const LogoWrapper = styled.div`
  box-flex: 1;
  flex: 1 1 120px;
  padding: 10px 0;
  height: 80px;
  display: flex;
  align-items: center;
  box-sizing: content-box;
`

export const Logo = styled.img`
  margin: auto;
  width: 100%;
  height: 60px;
`
