import React, { useState } from "react"
import { Link } from "gatsby"

//Components
import Container from "@material-ui/core/Container"
import Navigation from "components/Navigation"
import Hamburger from "components/Hamburger"
import MobileMenu from "components/MobileMenu"

//Styles
import {
  HeaderWrapper as Wrapper,
  HeaderInner as Inner,
  LogoWrapper,
  Logo,
} from "./Header.styles"

//Hooks
import { useSiteConfigQuery } from "hooks/useSiteConfigQuery"

const Header = () => {
  const siteConfig = useSiteConfigQuery()
  const [menuOpen, setMenuOpen] = useState(false)

  return (
    <Wrapper id="header">
      <Container maxWidth="xl">
        <Inner>
          <LogoWrapper>
            <Link to="/">
              <Logo src={siteConfig.logo.publicURL} alt="logo" />
            </Link>
          </LogoWrapper>
          <Hamburger menuOpen={menuOpen} setMenuOpen={setMenuOpen} />
          <MobileMenu menuOpen={menuOpen} items={siteConfig.menu} />
          <Navigation items={siteConfig.menu} />
        </Inner>
      </Container>
    </Wrapper>
  )
}

export default Header
