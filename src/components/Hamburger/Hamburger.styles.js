import styled from "styled-components"
import breakpoints from "styles/Breakpoints"

export const HamburgerIcon = styled.div`
  display: block;
  width: 30px;
  z-index: 1000;
  cursor: pointer;
  float: right;

  :after,
  :before,
  div {
    background-color: ${props => props.theme.colors.textWhite};
    border-radius: 2px;
    content: "";
    display: block;
    height: 4px;
    margin: 5px 0;
    transition: all 0.2s ease-in-out;
  }

  :before {
    transform: ${props =>
      props.menuOpen ? "translateY(9px) rotate(45deg)" : ""};
  }

  :after {
    transform: ${props =>
      props.menuOpen ? "translateY(-9px) rotate(-45deg)" : ""};
  }

  div {
    transform: ${props => (props.menuOpen ? "scale(0)" : "")};
  }

  @media screen and (${breakpoints.md}) {
    display: none;
  }
`
