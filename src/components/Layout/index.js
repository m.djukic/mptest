import React from "react"
import { ThemeProvider } from "styled-components"
import { MuiThemeProvider } from "@material-ui/core/styles"

//Components
import Header from "components/Header"
import Footer from "components/Footer"

/*import Hero from "components/Hero"
import About from "components/About"
import Carousel from "components/Carousel"
import Columns from "components/Columns"
import Living from "components/Living"
import HowItWorks from "components/HowItWorks"
import Brochure from "components/Brochure"
import Newsletter from "components/Newsletter"
import Copyright from "components/Copyright"*/

//Styles
import { GlobalStyles } from "styles/GlobalStyles"
import { DefaultTheme } from "styles/DefaultTheme"
import { MuiDefaultTheme } from "styles/MuiDefaultTheme"

const Layout = ( { children } ) => {
  return (
    <MuiThemeProvider theme={MuiDefaultTheme}>
      <ThemeProvider theme={DefaultTheme}>
        <GlobalStyles />
        <Header />
        { children }
        <Footer />
      </ThemeProvider>
    </MuiThemeProvider>
  )
}

export default Layout
