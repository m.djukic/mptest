import React from "react"

// Styles
import {
  HeroWrapper as Wrapper,
  Background,
  BackgroundOverlay,
  Content,
} from "./HeroImage.styles"

const HeroImage = ({ fluid, title, className, children }) => (
  <Wrapper>
    <BackgroundOverlay>
      <Background fluid={fluid} title={title} />
    </BackgroundOverlay>
    <Content className={className}>{children}</Content>
  </Wrapper>
)

export default HeroImage
