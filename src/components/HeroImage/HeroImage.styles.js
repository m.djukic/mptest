import styled from "styled-components"
import Img from "gatsby-image"

export const HeroWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100vh;
  overflow: hidden;
`

export const Background = styled(Img)`
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: -1;
`

export const BackgroundOverlay = styled.div`
  height: 100%;
  width: 100%;
  background-color: #00000080;
  z-index: 0;
`

export const Content = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 2;
`
