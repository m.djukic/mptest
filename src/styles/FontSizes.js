const fontSizes = {
  fontH1: "64px",
  fontH1S: "48px",
  fontH2: "32px",
  fontH3: "24px",
  fontH4: "16px",
  fontS: "14px",
  fontM: "16px",
  fontL: "18px",
}

export default fontSizes