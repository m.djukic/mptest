import { createGlobalStyle } from "styled-components"

// Components
import LatoRegular from "fonts/Lato-Regular.woff"
import LatoBold from "fonts/Lato-Bold.woff"
import breakpoints from "./Breakpoints"

export const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'Lato';
    src: local('Lato Regular'), local('LatoRegular'),
      url(${LatoRegular}) format('woff');
    font-style: normal;
  }

  @font-face {
    font-family: 'Lato';
    src: local('Lato Bold'), local('LatoBold'),
      url(${LatoBold}) format('woff');
    font-style: bold;
  }

  body {
    background: ${props => props.theme.colors.background};
    color: ${props => props.theme.colors.textDark};
    font-family: "Lato", Arial, Helvetica, sans-serif;
    margin: 0;
    padding: 0;
    overflow-x: hidden;
  }

  h1, h2, h3, h4, h5, h6, p  {
    margin: 0;
    padding: 0;
    border: 0;
  }

  h1 {
    font-size: ${props => props.theme.fontSizes.h1Mobile};
    font-weight: bold; 
    font-family: "Lato";
    margin: 0 0 20px 0;
    
    @media screen and (${breakpoints.lg}) {
      font-size: ${props => props.theme.fontSizes.h1};
    }
  }

  h2 {
    font-size: ${props => props.theme.fontSizes.h2};
    line-height: 1.3;    
    font-family: "Lato";
    font-weight: bold;
    margin: 0 0 20px 0;
  }

  h3 {
    font-size: ${props => props.theme.fontSizes.h4};
    line-height: 1.3;
    font-family: "Lato";
    font-weight: bold;
    margin: 0 0 10px 0;
  }

  h4 {
    font-size: ${props => props.theme.fontSizes.h4};
    line-height: 1.3;
    font-family: "Lato";
    font-weight: bold;
    color: ${props => props.theme.colors.textYellow};
    margin: 0 0 30px 0;
  }

  p {
    font-size: ${props => props.theme.fontSizes.textMedium};
    line-height: 1.6;
    font-family: "Lato";
    font-weight: normal;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  ul {
    margin: 0;
    padding: 0;
  }

  li {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }
`
