import colors from "./Colors"
import fontSizes from "./FontSizes"

export const DefaultTheme = {
  colors: {
    background: colors.white,
    menuBackground: colors.menu,
    footerBackground: colors.black,
    yellowBackground: colors.yellow,
    textWhite: colors.white,
    textDark: colors.black,
    textYellow: colors.yellow,
  },
  fontSizes: {
    h1: fontSizes.fontH1,
    h1Mobile: fontSizes.fontH1S,
    h2: fontSizes.fontH2,
    h3: fontSizes.fontH3,
    h4: fontSizes.fontH4,
    textSmall: fontSizes.fontS,
    textMedium: fontSizes.fontM,
    textLarge: fontSizes.fontL,
  },
}