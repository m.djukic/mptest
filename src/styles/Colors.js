const colors = {
  white: "#fff",
  black: "#171717",
  yellow: "#FFA700",
  grey: "#F8F8F8",
  menu: "#171717E6",
}

export default colors