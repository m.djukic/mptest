import { useStaticQuery, graphql } from "gatsby"

export const useFooterQuery = () => {
  const data = useStaticQuery(graphql`
    query FooterQuery {
      markdownRemark(frontmatter: {type: {eq: "footer"}}) {
        frontmatter {
          items {
            id
            name
            link
          }
        }
      }
    }
  `)
  return data.markdownRemark.frontmatter
}
