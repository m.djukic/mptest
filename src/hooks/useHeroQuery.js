import { useStaticQuery, graphql } from "gatsby"

export const useHeroQuery = () => {
  const data = useStaticQuery(graphql`
    query HeroQuery {
      markdownRemark(frontmatter: { type: { eq: "hero" } }) {
        frontmatter {
          heroImage {
            relativePath
            extension
            publicURL
            childImageSharp {
              fluid(maxWidth: 1920) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          heroImageText
          heroImageParagraph
          heroDiscoverText
          heroDiscoverLink
        }
      }
    }
  `)

  return data.markdownRemark.frontmatter
}