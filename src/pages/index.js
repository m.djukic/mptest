import React from 'react'
import { graphql } from 'gatsby'

//Components
import Layout from 'components/Layout'
import SEO from 'components/seo'
import Hero from 'components/Hero'

const IndexPage = () => 
  <Layout>
    <SEO title="Home" />
    <Hero />
  </Layout>

export default IndexPage

export const query = graphql`
  query {
    allImageSharp {
      edges {
        node {
          id
          fluid(maxWidth: 200, maxHeight: 200) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`