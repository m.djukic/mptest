---
type: "howitworks"
HowItWorksHeader: "Hoe Ygo Werkt"
items:
  - id: "howitworks1"
    img: "../images/works1.png"
    header: "Kijken"
    paragraph: "Ontdek het ruime aanbod op onze afdelingen woonaccessoires, zitten, wonen, slapen en Pronto Wonen."
  - id: "howitworks2"
    img: "../images/works2.png"
    header: "Kiezen"
    paragraph: "Diverse woonstijlen van landelijk tot industrieel of modern tonen voor ieder wat wils. En dat aan de scherpste prijs!"
  - id: "howitworks3"
    img: "../images/works3.png"
    header: "Meenemen"
    paragraph: "Gevonden wat je zocht? Ga het meteen afhalen in ons magazijn, huur een YGO camionette of laat het bij je thuis leveren."
---
