---
type: "footer"
items: 
  - id: "phone"
    name: "123123123"
    link: "/"
    img: "../images/phone.svg"
  - id: "email"
    name: "book@book.com"
    link: "/"
  - id: "c1r3"
    name: "Novi Sad 21000"
    link: "/"
---
