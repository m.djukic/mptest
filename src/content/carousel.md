---
type: "carousel"
carouselHeader: "Winkelen Per Categorie"
items:
  - id: "carousel1"
    img: "../images/shop-item1.png"
    name: "Accessoires"
    link: "/"
  - id: "carousel2"
    img: "../images/shop-item2.png"
    name: "Ziten"
    link: "/"
  - id: "carousel3"
    img: "../images/shop-item3.png"
    name: "Wonen"
    link: "/"
  - id: "carousel4"
    img: "../images/shop-item4.png"
    name: "Slapen"
    link: "/"
  - id: "carousel5"
    img: "../images/shop-item5.png"
    name: "Verlichting"
    link: "/"
---
