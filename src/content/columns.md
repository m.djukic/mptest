---
type: "columns"
items:
  - id: "column1"
    img: "../images/living-col1.png"
    header: "Laat je inspireren"
    paragraph: "Door al onze woontrends op de verschillende afdelingen."
  - id: "column2"
    img: "../images/living-col2.png"
    header: "Laat je verrassen"
    paragraph: "Door onze shop-in-shop Pronto Wonen (bevindt zich op het 2e verdiep)"
  - id: "column3"
    img: "../images/living-col3.png"
    header: "Need a break?"
    paragraph: "Door al onze woontrends op de verschillende afdelingen."
---
