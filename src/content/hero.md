---
type: "hero"
heroImage: "../images/hero.jpg"
heroImageText: "Aliquam purus sit amet luctus venenatis"
heroImageParagraph: "Et sollicitudin ac orci phasellus egestas tellus. Pellentesque adipiscing commodo elit at imperdiet dui accumsan. Nunc sed augue lacus viverra vitae congue eu consequat."
heroDiscoverText: "Ontdek"
heroDiscoverLink: "/"
---
