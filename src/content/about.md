---
type: "about"
aboutImage: "../images/about.png"
aboutHeader: "YGO Interieur & accessoires is het nieuwste woonwinkelconcept van Lier dat zich bevindt op de shoppingsite van YGO."
aboutText: "YGO Interieur & accessoires is het nieuwste woonwinkelconcept van Lier dat zich bevindt op de shoppingsite van YGO. Je vindt er 12 000m2 aan woonaccessoires, slaapkamers, boxsprings, dressings, sofa’s, eetkamers, shop-in-shop Pronto Wonen en nog veel meer."
aboutTextYellow: "Kom het zelf ontdekken en laat je inspireren door ons uitgebreid aanbod! Zoek je nog een verrassend cadeau? Maak iemand blij met een cadeaubon van YGO Interieur & accessoires."
aboutBtnText: "Meer Over Ygo"
aboutBtnLink: "/"
---
