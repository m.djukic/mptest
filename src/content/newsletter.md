---
type: "newsletter"
newsletterImage: "../images/newsletter.png"
newsletterHeader: "Schrijf je in op onze nieuwbrief"
newsletterText: "Blijf als eerste op de hoogte van woonideeën, acties en inspiratie"
newsletterBtnLink: "/"
newsletterBtnText: "Schrijf"
---
